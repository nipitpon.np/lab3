import java.util.Scanner;
public class Problem5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String textInput,textInputCheck;
        while(true){
            System.out.print("Please input : ");
            textInput = sc.nextLine();
            textInputCheck = textInput.toLowerCase();
            if(textInputCheck.equals("bye")) break;
            System.out.println(textInput);
        }
        System.out.println("Exit Program");
        sc.close();
    }
}
